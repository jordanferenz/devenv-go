FROM golang:1.11.1-alpine3.8
RUN sed -i -e 's/v[[:digit:]]\.[[:digit:]]/edge/g' /etc/apk/repositories
RUN apk upgrade --update-cache --available
RUN apk --no-cache add ca-certificates curl git python3 neovim neovim-lang nodejs
RUN apk add --virtual deps build-base gcc python3-dev yarn && pip3 install neovim && yarn global add neovim && apk del deps
RUN curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
RUN go get -u github.com/mdempsky/gocode
RUN apk add build-base gcc python3-dev make
COPY init.vim /root/.config/nvim/init.vim
RUN nvim --headless +PlugInstall +UpdateRemotePlugins +qa
#RUN apk add --virtual deps build-base gcc python3-dev make && nvim --headless +PlugInstall +UpdateRemotePlugins +qa && apk del deps
WORKDIR /devel
ENTRYPOINT ["nvim"]
